<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BIOMEDICA</title>
    <!--FONT ASSISTANT-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Assistant:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <!--CUSTOM CSS-->
    <link rel="stylesheet" href="static/css/styles.css">
    <!--ICONS CSS-->
    <link rel="stylesheet" type="text/css" href="static/css/Icons/font/flaticon.css">
</head>
<body>
    <!--MENU-->
    <div class="sticky">
    <header class="container-cont">
        <nav class="nav d-flex a-i-center j-c-between pad-15-0 ">
            <div >
                <a href="/"><img src="static/images/logo.png" alt="biomedica LOGO" class="nav-logo"></a>
            </div>
            <div class="nav-menu d-flex">
                <ul class="nav-items d-flex j-c-between"  >
                    <li>
                        <a href="/" class="a f-family f-18px w-600 red-1">INICIO</a>
                    </li>
                    <li>
                        <a href="/" class="a f-family f-18px w-600 red-1">ECOGRAFOS</a>
                    </li>
                    <li>
                        <a href="/" class="a f-family f-18px w-600 red-1">SOPORTE</a>
                    </li>
                    <li>
                        <a href="/" class="a f-family f-18px w-600 red-1">NOSOTROS</a>
                    </li>
                    <li>
                        <a href="/" class="a f-family f-18px w-600 red-1">COTIZACIÓN</a>
                    </li>
                </ul>

            </div>
        </nav>
    </header>
    <hr class="separador">
    </div>