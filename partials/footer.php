<!--Footer-->
<footer  class="footer bg-overlay" >
    <!---<img src="solo-equipos-ban.jpg" alt="Banner Footer" class="bannerSect">-->
    <div class="container-footer d-flex a-i-center j-c-center f-family">
        <div class="grid-footer d-grid j-c-between">
            <div id="contactenos" class="colum">
                <h1 class="f-15px white-2 w-400"><span class="w-800 red-2 pad-right-10">|</span>CONTACTENOS</h1>
                    <div class="row d-flex pad-15-0 ">
                        <div class="footer-icon"><i class="flaticon-pin red-2 f-20px"> </i> </div>
                        <div class="text">
                            <h2 class="f-15px grey-1">Dirección:</h2>
                            <a href="/" class="f-15px white-3 a">Cal. Teniente Aristides del <br/>
                            Carpio Muñoz 1556,<br/> Lima - Lima - Perú</a>
                        </div>
                    </div>
                     <div class="row d-flex ">
                         <div class="footer-icon"><i class="flaticon-telephone red-2 f-20px" ></i> </div>
                        <div class="text">
                            <h2 class="f-15px grey-1">Teléfono:</h2>
                             <a href="/" class="f-15px white-3 a">+51 123 456 789</a>
                        </div>
                     </div>
                <div class="row d-flex pad-15-0">
                    <div class="footer-icon"><i class="flaticon-telefono-inteligente red-2 f-20px" ></i> </div>
                    <div class="text">
                         <h2 class="f-15px grey-1">Whatsapp:</h2>
                        <a href="/" class="f-15px white-3 a">+51 123 456 789</a>
                    </div>
                </div>
                <div class="row d-flex">
                    <div class="footer-icon"><i class="flaticon-email red-2 f-20px"></i> </div>
                    <div class="text">
                        <h2 class="f-15px grey-1">Correo Electrónico:</h2>
                        <a href="/" class="f-15px white-3 a">ventas@biomedica.pe</a>
                    </div>
                </div>
            </div>
            <div class="colum">

                    <ul  id="sect-legales"  class="f-15px margin-content-f-2">
                        <h1 class="f-15px white-2 w-400 pad-but-15"><span class="w-800 red-2 pad-right-10">|</span>ENLACES LEGALES</h1>
                        <li class="pad-but-15"><a href="/" class="white-1 a">Autorizaciones</a></li>
                        <li class="pad-but-15"><a href="/" class="white-1 a">Politica de Privacidad</a></li>
                        <li class="pad-but-15"><a href="/" class="white-1 a">Terminos y condiciones de Uso</a></li>
                    </ul>
            </div>
            <div class="colum">
                <ul class="f-15px margin-content-f-3 ">
                    <h1 class="f-15px white-2 w-400 pad-but-15"><span class="w-800 red-2 pad-right-10">|</span>ECOGRAFOS</h1>
                    <li class="pad-but-15"><a href="/" class="white-1 a">Minisono</a></li>
                    <li class="pad-but-15"><a href="/" class="white-1 a">E-CUBE i7</a></li>
                    <li class="pad-but-15"><a href="/" class="white-1 a">E-CUBE 8</a></li>
                    <li class="pad-but-15"><a href="/" class="white-1 a">E-CUBE 12</a></li>
                    <li class="pad-but-15"><a href="/" class="white-1 a">E-CUBE 15</a></li>
                </ul>
            </div>
            <div class="colum margin-content-logo">
                <img id="logo-footer" src="static/images/logo-con-lema-en-blanco.png" alt="logo footer">
                <div class="d-flex a-i-center j-c-between pad-15-0">
                    <div><a class="a" href="/" ><i class="flaticon-facebook white-1 f-30px"></i></a></div>
                    <div><a class="a" href="/" ><i class="flaticon-instagram white-1 f-30px"></i></a></div>
                    <div><a class="a" href="/" ><i class="flaticon-youtube white-1 f-30px"></i></a></div>
                    <div><a class="a" href="/" ><i class="flaticon-twitter white-1 f-30px"></i></a></div>
                    <div><a class="a" href="/" ><i class="flaticon-linkedin-logo white-1 f-30px"></i></a></div>
                </div>

            </div>
        </div>

    </div>
</footer>
<footer>
    <div class="footer-2 d-flex ">
        <div id="footer-2"  class="container-cont d-flex a-i-center j-c-between">
            <p class="grey-1 f-family f-15px">©2021 Hecho por DROGUERIA BIOMEDICA PERUANA SAC | Todos los derechos reservados</p>
            <ul id="menu-footer-2" class="d-flex a-i-center ">
                <li><a class="pad-right-10 white-1 f-family a" href="/">Inicio</a></li>
                <li><p class="pad-right-10 white-1 f-family a">|</p></li>
                <li><a class="pad-right-10 white-1 f-family a" href="/">Soporte</a></li>
                <li><p class="pad-right-10 white-1 f-family a">|</p></li>
                <li><a class="pad-right-10 white-1 f-family a" href="/">Nosotros</a></li>
            </ul>
        </div>
    </div>
</footer>
