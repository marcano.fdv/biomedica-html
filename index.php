<?php include ("./partials/header.php") ?>
<link rel="stylesheet" href="static/css/styles.css">
<!--VIDEO HEADER-->
<video autoplay muted loop id="myVideo">
    <source src="static/videos/PRÓXIMAMENTE.mp4" type="video/mp4">
</video>
<!--SECTION 1-->
<section id="section-1" class="d-flex j-c-center b-img-fixed">
    <div class="container-section d-flex j-c-end a-i-center">
        <div class="cont-half back-c-white txt-center d-flex j-c-center f-direc-colum">
            <h1 class="f-family f-39px w-800 red-1 t-shadow pad-15-0">PASION POR EL ULTRASONIDO</h1><br>
            <p class="f-family f-18px grey-2 marg-side-40px">Con la determinación que más apasionados obtengan mejores resultados en el
                área diagnóstica con mayores avances tecnológicos: el Ultrasonido Diagnóstico.</p><br>

            <p class="f-family f-18px grey-2 marg-side-40px"> Por ello somos muy selectivos en cuanto a los ecografos que
                tenemos para ti, con tecnología de vanguardia, con la mejor relación calidad/precio;
                pero, sobre todo preparados para el uso exigente que impone la productividad actual.</p><br>
            <p class="f-family f-20px grey-3 w-600"><strong>Conoce más sobre los ecografos que tenemos para ti.</strong> </p>

            <div class="grid-2-colum d-grid">
                <div class="gridLogo d-flex j-c-center a-i-center">
                    <a href="/"><img class="img-grey-hover" style="padding: 30px 0px 0px 0px;" src="static/images/equipos-logos-alpi-07.png" alt="Logo Minisono"></a>
                </div>
                <div class="gridLogo">
                    <a href="/"><img class="img-grey-hover" src="static/images/equipos-logos-alpi-08.png" alt="Logo e cube i7"></a>
                </div>
                <div class="gridLogo">
                    <a href="/"><img class="img-grey-hover" src="static/images/equipos-logos-alpi-01.png" alt="Logo e cube 8"></a>
                </div>
                <div class="gridLogo">
                    <a href="/"><img class="img-grey-hover" src="static/images/equipos-logos-alpi-02.png" alt="Logo e cube 15"></a>
                </div>
            </div>
        </div>
    </div>
</section>
<hr class="separador">
<!--SECTION 2-->
<section id="section-2" class="d-flex a-i-center txt-center j-c-evenly back-c-white" >
    <div class="container-cont  txt-center d-flex f-direc-colum a-i-center j-c-center">
        <h1  id="title-sect" class="pad-15-0 f-family f-39px w-800 red-2">
            Cuidamos tu Inversión
        </h1>
        <p class="f-family grey-2 f-20px">Los apasionados por la ecografía y usuarios de ecografos <strong>ALPINION</strong>,
            obtienen los mejores beneficios por ser parte de nuestra gran <strong>FAMILIA:</strong>
        </p>
        <div class="grid-3-colum d-grid">
            <div class="grid-element d-flex f-direc-colum j-c-evenly a-i-center txt-center">
                <img class="icon" src="static/images/garantia.png" alt="">
                <h2 class="f-family f-20px red-2">Garantía</h2>
                <p class="f-family grey-2 f-18px">¡Todos nuestros ecografos cuentan con una garantía de fabrica por 36 meses!</p>
            </div>
            <div class="grid-element d-flex f-direc-colum j-c-evenly a-i-center txt-center">
                <img class="icon" src="static/images/soporte-op1.png" alt="">
                <h2 class="f-family f-20px red-2">Soporte</h2>
                <p class="f-family grey-2 f-18px">Atendemos tus consultas sobre tu equipo de manera oportuna, cuando lo requiera!</p>
            </div>
            <div class="grid-element d-flex f-direc-colum j-c-evenly a-i-center txt-center">
                <img class="icon" src="static/images/Mantenimiento.png" alt="">
                <h2 class="f-family f-20px red-2">Mantenimiento</h2>
                <p class="f-family grey-2 f-18px">Mantenimientos semestrales preventivos, sin costo, durante todo el periodo de garantía</p>
            </div>

        </div>
        <hr class="space">
    </div>
</section>
<!--SECTION 3-->
<section  id="section-3"   class="bg-overlay">
    <div id="form-section"  class="container-cont h-inherit d-flex a-i-center j-c-between">
        <div id="cont-half-3" class="cont-half txt-center d-flex f-direc-colum j-c-center">
            <h1  id="title-sect" class="pad-15-0 f-family f-39px w-800 white-2">
                ¿Ya te decidiste?
            </h1>
            <p class="f-family line-h pad-15-0 f-18px w-400 white-1">¡Ahora comunícate con nosotros!<br></p>
            <p class="f-family line-h pad-15-0 f-18px w-400 white-1">Te brindaremos un asesoramiento profesional para que encuentres
                un ecografo al mejor precio, que se adapte a tus necesidades y presupuesto.<br></p>
            <p class="f-family line-h pad-15-0 f-18px w-400 white-1">Completa el formulario y en breve nos contactaremos contigo
                para brindarte la atención personalizada que mereces…<br> </p>
        </div>
        <div id="form" class="cont-half d-flex a-i-center j-c-center">
            <form class="f-family white-1 f-15px">
                <input class="input white-1" type="text" name="nombre" placeholder="Nombre Completo*" required><br/>
                <input class="input white-1" type="text" name="telf" placeholder="Número de teléfono" required><br/>
                <input class="input white-1" type="text" name="email" placeholder="Correo Electrónico" required><br/>
                <p>¿Qué equipo le interesa más?</p>
                <select name="Seleccione">
                    <option value="---">---</option>
                    <option value="portatil">Potátil</option>
                    <option value="rodable">Rodable</option>
                </select>
                <p>¿Con que transductores te gustaria tu equipo?</p>
                <div class="d-flex a-i-center marg-top-label checkbox-width">
                    <input class="marg-check-form" type="checkbox" name="transductores[]" value="convexo">Convexo</input>
                    <input class="marg-check-form" type="checkbox" name="transductores[]" value="lineal">Lineal</input>
                    <input class="marg-check-form" type="checkbox" name="transductores[]" value="endocvitario">Endocavitario</input>
                    <input class="marg-check-form" type="checkbox" name="transductores[]" value="volumetrico">Volumetrico</input>
                    <input class="marg-check-form" type="checkbox" name="transductores[]" value="phased">Phased Array</input>
                </div>
                <p>¿Te gustaría añadir algún accesorio a tu cotización? Mira nuestras opciones</p>
                <div class="d-flex a-i-center pad-but-15 marg-top-label checkbox-width">
                    <input class="marg-check-form" type="checkbox" name="perifericos[]" value="impresora">Impresora</input>
                    <input class="marg-check-form" type="checkbox" name="perifericos[]" value="gel">Gel</input>
                    <input class="marg-check-form" type="checkbox" name="perifericos[]" value="ups">UPS</input>
                    <input class="marg-check-form" type="checkbox" name="perifericos[]" value="otros">Otros</input>
                </div>
                <button class="form-button f-family white-1 f-18px">Enviar</button>
            </form>
        </div>
    </div>
</section>
<!--SECTION 4-->
<section id="section-4" class="d-flex f-direc-colum j-c-evenly">
    <div id="container-4" class="container-cont txt-center f-family">
        <h1  id="title-sect-4" class="pad-15-0 f-39px red-2 w-600">¡Ellos confian en nosotros!</h1>
        <p id="text-sect-4"  class="f-20px grey-2">Escuelas y sociedades médicas dedicadas a la enseñanza con Buenas Prácticas
            del Ultrasonido diagnóstico</p>
        <div class="grid-5-colum d-grid a-i-center">
            <div class="Logo-element">
                <a href="/"><img src="static/images/Ce-02.png" alt="Centrus" class="img-grey-hover"></a>
            </div>
            <div class="logo-element">
                <a href=""><img src="static/images/ASOPUC-sin-fondo.png" alt="Asopuc" class="img-grey-hover"></a>
            </div>
            <div class="logo-element">
                <a href="/"><img src="static/images/UROLOGIA-sin-fondo.png" alt="Urologia" class="img-grey-hover"></a>
            </div>
            <div class="logo-element">
                <a href="/"><img src="static/images/Logo-Spaar-2016.png" alt="Spaar" class="img-grey-hover"></a>
            </div>
            <div class="logo-element">
                <a href="/"><img src="static/images/ecosermedic-sin-fondo.png" alt="Ecosermedic" class="img-grey-hover"> </a>
            </div>
        </div>
    </div>
</section>
<?php include ("./partials/footer.php") ?>
